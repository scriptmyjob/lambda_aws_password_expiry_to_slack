# AWS expiry notification

This is a simple python lambda function to run a report on users who are about to have their iam password expire:

# Configuration Steps:

Copy secret's example and provide a webhook.
```
cp -a secrets.tf{-example,}
```

Terraform initialization:
```
terraform init
```

Run Terraform on the `Terraform/` directory:
```
terraform apply Terraform/
```

This will run at 8 AM CST everyday, and can be modified in cron.tf if desired.
