#!/usr/bin/env python
# Written by: Robert J.

import csv
from datetime import datetime, timedelta
import boto3
import sys, os
import time
import json
import logging

from urllib2 import Request, urlopen, URLError, HTTPError

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Global Vars #######################
#######################################

HOOK_URL        = os.environ['HOOK_URL']
SLACK_CHANNEL   = os.environ['SLACK_CHANNEL']
ICON_EMOJI      = os.environ['ICON_EMOJI']
USERNAME        = os.environ['USERNAME']

#######################################
### Boto3 Settings ####################
#######################################

iam         = boto3.client('iam')

#######################################
### Main Function #####################
#######################################

def main():
    default_message = 'There are no expiring passwords.'

    report          = get_report()
    account         = get_account()
    passwd_age      = get_password_age(account)
    output          = read_data(report, passwd_age, default_message)

    if output != default_message:
        message         = generate_message(account, output, passwd_age)
        json_payload    = generate_payload(
                                alarm_name='Password Expiration',
                                reason=message,
                                fallback=message,
                                title_link='https://console.aws.amazon.com/iam/home#/users',
                                account=account
                          )
        post_payload(json_payload)

    logger.info("Exiting Lambda Function.")

    return output


#######################################
### Program Specific Functions ########
#######################################

def get_report():
    logger.info("Generating Report...")

    state = iam.generate_credential_report()['State']
    logger.info(state)

    while state != 'COMPLETE':
        state = iam.generate_credential_report()['State']
        logger.info("Report in progress: {}".format(state))
        time.sleep(0.25)

    logger.info("Pulling Report...")
    data = iam.get_credential_report()['Content']

    return data


def get_account():
    paginator = iam.get_paginator('list_account_aliases')
    try:
        logger.info("Trying to get the account name.")
        aliases = [
            response['AccountAliases'][0] for response in paginator.paginate()
        ]
        alias = aliases[0]
    except:
        logger.info("Unable to get account name, failing back to get account number.")
        alias = get_account_number()

    return alias

def get_account_number():
    account_number = iam.CurrentUser().arn.split(':')[4]

    return account_number


def get_password_age(account):
    logger.info("Pulling current password policy...")

    try:
        passwd_age = iam.get_account_password_policy()['PasswordPolicy']['MaxPasswordAge']
    except KeyError:
        message = 'IAM Policy Password Expiration has been disabled.'
        json_payload    = generate_payload(message)
        post_payload(json_payload)
        sys.exit()

    logger.info(str(passwd_age) + " days")

    return passwd_age


def read_data(data, passwd_age, default_message):
    value = ''

    logger.info("Parsing Report...")
    for i in csv.DictReader(data.split()):
        status = None
        logger.info(i['user'])

        if i['password_last_changed'] == 'not_supported':
            continue

        if i['password_last_changed'] == 'N/A':
            continue

        # date information for parsing
        fmt                 = '%Y-%m-%dT%H:%M:%S+00:00'
        date                = i['password_last_changed']

        # date changed and now
        changed             = datetime.strptime(date, fmt)
        now                 = datetime.now()

        # date difference
        diff                = now - changed
        diff_days           = diff.total_seconds()/3600/24
        expiration          = changed + timedelta(passwd_age)

        passwd_notification = passwd_age - 7

        if diff_days >= passwd_age:
            status = 'HAS'

        if diff_days >= passwd_notification :
            status = 'WILL'

        if status:
            new_content = '{:>20}\'s password {} expire at {}.'.format(
                    i['user'],
                    status,
                    expiration
                ) + \
                "\n"

        if new_content:
            value += new_content
            logger.info("Action Required:\n" + new_content)

    if not value:
        value = 'There are no expiring passwords.'

    return value


def generate_message(account, out, passwd_age):
    logger.info("Message:\n"        + out)
    logger.info("Account: "         + account)
    logger.info("Password Age: "    + str(passwd_age))
    if out != 'There are no expiring passwords.':
        message = "Current policies have passwords expiring every " + \
            str(passwd_age) + " days" + \
            "\n" + "\n" + \
            out

    return message


def generate_payload(
        state='WARNING',
        fallback="fallback not set.",
        alarm_name='alarm_name not set.',
        old_state='INSUFFICIENT_DATA',
        reason='reason not set.',
        title_link="https://us-west-2.console.aws.amazon.com",
        metric_name=None,
        resource=None,
        account=None
    ):

    if state == "ALARM":
        color = "#A30200"
    elif state == "OK":
        color = "#2eb886"
    elif state == "WARNING":
        color = "#daa038"
    else:
        color = "#dddddd"

    slack_message   = {
        "attachments": [
           {
                "fallback": reason,
                "color": color,
                "title": alarm_name,
                "title_link": title_link,
                "fields": [
                    {
                        "title": "State",
                        "value": "%s -> %s" % (old_state, state),
                        "short": False
                    },
                    {
                        "title": "Reason",
                        "value": reason,
                        "short": False
                    }
                ]
            }
        ],
        'channel': SLACK_CHANNEL,
        'username': USERNAME,
        'icon_emoji': ICON_EMOJI
    }

    if metric_name:
        slack_message["attachments"][0]["fields"].append(
            {
                "title": "Metric Name",
                "value": metric_name,
                "short": False
            }
        )

    if resource:
        slack_message["attachments"][0]["fields"].append(
            {
                "title": "Affected Resource",
                "value": resource,
                "short": False
            }
        )

    if account:
        slack_message["attachments"][0]["fields"].append(
            {
                "title": "Account",
                "value": account,
                "short": False
            }
        )

    return slack_message


def post_payload(json_payload):
    req = Request(HOOK_URL, json.dumps(json_payload))

    try:
        response = urlopen(req)
        response.read()
        logger.info("Message posted to %s", json_payload['channel'])
    except HTTPError as e:
        error("Request failed: {} {}".format(e.code, e.reason))
    except URLError as e:
        error("Server connection failed: {}".format(e.reason))


def error(message, code=1):
    logger.error(message)
    sys.exit(code)


#######################################
### Execution #########################
#######################################

if __name__ == "__main__":
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    main()

def execute_me_lambda(event, context):
    out = main()
    return out
